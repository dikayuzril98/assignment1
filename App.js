import React, {Component} from 'react'
import {ScrollView, StyleSheet, View, Text} from 'react-native'
import Axios from 'axios'

import Movies from './src/Component/Movies'

class App extends Component {
  state={
    listMovies:[],
  }
  getList = async () => {
    const response = await Axios.get
    ('http://www.omdbapi.com/?s=avenger&apikey=997061b4&')
    if (response.data.Response) {
      this.setState({
        listMovies:(response.data.Search)
      })
    }
  }
  componentDidMount() {
    this.getList()
  }
  render(){
    return(
      <ScrollView>
        <View style={styles.contaier}>  
            <Text style={{fontWeight:'bold', fontSize: 20, alignSelf:'center',paddingHorizontal: 15,paddingTop:15}}>
              MOVIE LIST
            </Text>
              {this.state.listMovies.map ((item,index) =>{
                  return (
                    <View key={index}>
                      <Movies title={item.Title} year={item.Year} image={item.Poster} type={item.Type}/>
                    </View>
                  )
                })} 
        </View>
      </ScrollView>
    )
  }
}
export default App

const styles = StyleSheet.create({
  contaier:{
    flex:1,
    backgroundColor:'#006400'
  }
})
