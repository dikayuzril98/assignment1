import React, { Component } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'

class Movies extends Component {
    render() {
        const {title, year,image, type}= this.props
        return (
            <View style={styles.cardMovies}>
                <View style= {{flexDirection:'row'}}>
                    <Image style={{height:130, width:90, marginRight:15, borderRadius:10}} source={{uri: image}} />
                    <View style={{marginTop:15, flexwrap:'wrap', maxWidth:250}}>
                        <Text style={{fontSize: 15,fontWeight:'bold'}}>{title}</Text>
                        <Text Style={{fontSize:12, fontWeight:'bold'}}>{year}</Text>
                    </View>
                    <View style={{position:'absolute',end:0,bottom:0, margin: 15, borderWidth:1, paddingHorizontal:15, backgroundColor:'gold',borderRadius:10}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>{type}</Text>
                    </View>
                </View>
          </View>
        )
    }
}
export default Movies 

const styles = StyleSheet.create({
    cardMovies:{
        backgroundColor: '#228B22' ,
        padding:15,
        borderRadius:15,
        margin:5,
        borderBottomWidth:10,
        borderLeftWidth:4
      }

})
